from django.apps import AppConfig


class WC2018Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'wc2018'
