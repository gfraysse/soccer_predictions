from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

from . import views

app_name = 'wc2018'

urlpatterns = [
    path('', views.pronostics, name='pronostics'),
    path('pronostics/', views.pronostics, name='pronostics'),
    path('pronostiquer/', views.pronostiquer, name='pronostiquer'),
    path('teams/', views.teams, name='teams'),
    path('groups/', views.groups, name='groups'),
    path('resultats/', views.resultats, name='resultats'),
    path('knockout/', views.knockout, name='knockout'),
    path('classement/', views.classement, name='classement'),
    path('record/', views.record, name='record'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
