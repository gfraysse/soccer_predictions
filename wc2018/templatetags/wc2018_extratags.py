from django import template
from datetime import datetime, timedelta
from django.utils import timezone

register = template.Library()

@register.filter
def votetime_days(value):
    todays_date = timezone.now() + timedelta(minutes=60)
    then = value + timedelta(minutes=-135)
    if then <= todays_date:
        return 0
    return then - todays_date
