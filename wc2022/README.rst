=====
Soccer World Cup predictions
=====

soccer_predictions is a Django app to conduct Web-based predictions for the soccer World Cup. User can predict the score of a match up to 15 minutes before the match and are ranked by the app.
visitors can choose between a fixed number of answers.

Detailed documentation is in the "docs" directory.

Quick start
-----------

1. Add "wc2022" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'wc2022',
    ]

2. Include the polls URLconf in your project urls.py like this::

    path('wc2022/', include('wc2022.urls')),

3. Run ``python manage.py migrate`` to create the polls models.

4. Start the development server and visit http://127.0.0.1:8000/admin/
   to populate the world cup teams, groups and results (you'll need the Admin app enabled).

5. Visit http://127.0.0.1:8000/wc2022/ to participate in the poll.
