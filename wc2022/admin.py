from django.contrib import admin

# Register your models here.
from .models import Match, Round, Team, Result, Prono, Group

admin.site.register(Match)
admin.site.register(Round)
admin.site.register(Team)
admin.site.register(Group)
# admin.site.register(User)
admin.site.register(Result)
admin.site.register(Prono)
