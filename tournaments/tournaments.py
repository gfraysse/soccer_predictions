class Ranking:
    def __init__(self, team, group):
        self.team = team
        self.group = group
        self.gf = 0
        self.ga = 0
        self.gd = 0
        self.w = 0
        self.l = 0
        self.d = 0
        self.pld = 0
        self.pts = 0
        self.pos = 0
                
    def __str__(self):
        return self.team.name + ":" + str(self.pts)
    
class Tournament:
    def __init__(self):
        pass
