from django.db import models

import functools
import re

from .tournaments import Tournament, Ranking
from .Winner import Winner

class Worldcup32(Tournament):
    def __init__(self):
        pass
        
    def compare_rankings(self, r1, r2):
        # 1. Points obtained in all group matches;
        if r1.pts < r2.pts:
            return 1
        if r1.pts > r2.pts:
            return -1
    
        # From now on we assume both team have the same number of points
        assert(r1.pts == r2.pts)

        # 2. Goal difference in all group matches;
        if r1.gd < r2.gd:
            return 1
        if r1.gd > r2.gd:
            return -1
    
        # From now on we assume both team have the same goal difference
        assert(r1.gd == r2.gd)
    
        # 3. Number of goals scored in all group matches;
        if r1.gf < r2.gf:
            return 1
        if r1.gf > r2.gf:
            return -1

        #     Points obtained in the matches played between the teams in question;
        #     Goal difference in the matches played between the teams in question;
        #     Number of goals scored in the matches played between the teams in question;
        #     Fair play points in all group matches (only one deduction can be applied to a player in a single match):
        #         Yellow card: -1 point;
        #         Indirect red card (second yellow card): -3 points;
        #         Direct red card: -4 points;
        #         Yellow card and direct red card: -5 points;
        #     Drawing of lots.
        
        return 0
    
    def compute_group_rankings(self, results, matches, teams, groups):                
        r = []
        for group in groups.all():
            # print(group)
            ranking_group = {}
            for team in [group.team1, group.team2, group.team3, group.team4]:
                ranking_group[team.name] = Ranking(team, group)
            # print("ranking_group=", ranking_group)
            # for match in matches.all():
            for team in [group.team1, group.team2, group.team3, group.team4]:
                tmp = results.filter(match__team1_id=team.id)
                # print("  match=", match)
                #     print("    team=", team)
                for result in tmp:
                    try:
                        # if match.team1.name != team.name:
                        #     continue
                        # result = results.get(match__team1_id=team.id)
                        # print("      result=", result)
                        ranking_group[result.match.team1.name].gf += result.score_extratime_team1
                        ranking_group[result.match.team1.name].ga += result.score_extratime_team2
                        ranking_group[result.match.team1.name].gd += result.score_extratime_team1 - result.score_extratime_team2
                        
                        ranking_group[result.match.team2.name].gf += result.score_extratime_team2
                        ranking_group[result.match.team2.name].ga += result.score_extratime_team1
                        ranking_group[result.match.team2.name].gd += result.score_extratime_team2 - result.score_extratime_team1
                        
                        ranking_group[result.match.team1.name].pld += 1
                        ranking_group[result.match.team2.name].pld += 1
                        
                        if result.score_extratime_team1 > result.score_extratime_team2:
                            ranking_group[result.match.team1.name].w += 1
                            ranking_group[result.match.team1.name].pts += 3
                            ranking_group[result.match.team2.name].l += 1
                        elif result.score_extratime_team1 < result.score_extratime_team2:
                            ranking_group[result.match.team2.name].w += 1
                            ranking_group[result.match.team2.name].pts += 3
                            ranking_group[result.match.team1.name].l += 1
                        else: 
                            ranking_group[result.match.team2.name].d += 1
                            ranking_group[result.match.team2.name].pts += 1
                            ranking_group[result.match.team1.name].d += 1
                            ranking_group[result.match.team1.name].pts += 1
                           
                            
                    except Exception as e:
                        print(e)
                        continue
                
            for team in [group.team1, group.team2, group.team3, group.team4]:
                r.append(ranking_group[team.name])

        for group in groups.all():
            group_ranking = {}
            for team in [group.team1, group.team2, group.team3, group.team4]:
                for ranking in r:
                    if ranking.team.name == team.name:
                        group_ranking[team.name] = ranking
            decorated = [(g.pts, i, g) for i, g in enumerate(group_ranking.values())]
            gr = sorted(group_ranking.values(), key=functools.cmp_to_key(self.compare_rankings))
            # decorated.sort(reverse=True)
            # print(decorated)
            # gr = [gr for pts, i, gr in decorated]
            # print("gr=", gr)
            # for k, v in gr.items():
            for i in range(len(gr)):
                # print("group i=", v.team.name)
                team_name = gr[i].team.name
                # print("group i=", team_name)
                group_ranking[team_name].pos = i + 1

        return r
    
    def getMatchWinnerAndLoser(self, result, matches):
        # Team name is of format Mxy whre xy is the match number
        match_id = result.match.id
        try:
            for m in matches:
                # print("m=", m)
                # print("match_id=", m.match_id, match_id)
                if m.match.id == match_id:
                    # print(m)
                    # print("getMatchWinnerAndLoser", result, result.vainqueur)
                    if result.vainqueur == Winner.Team1:
                        return m.match.team1, m.match.team2
    
                    return m.match.team2, m.match.team1
        except Exception as e:
            print(e)
            return -1, -1

    def getGroupStageQualifiedTeams(self, match, rankings):
        # Team name is of format [WR]G where:
        # - W stand for Winner of the group
        # - R stand for Runner-up of the group
        # - G is the Group name
        g1 = match.team1.name[1]
        g2 = match.team2.name[1]
        t1 = -1
        t2 = -1
        for r in rankings:
            # print("r", r, r.group.name, r.pos)
            if r.group.name == g1:
                if (match.team1.name[0] == "W" and r.pos == 1) or (match.team1.name[0] == "R" and r.pos == 2):
                    t1 = r.team
                    # print(match, "t1=", t1)
            elif r.group.name == g2:
                if (match.team2.name[0] == "W" and r.pos == 1) or (match.team2.name[0] == "R" and r.pos == 2):
                    t2 = r.team
                    # print(match, "t2=", t2)
        return t1, t2

    def compute_knockout_stages(self, rounds, results, rankings, all_matches, result_func):
        matches = []
        ordered_rounds = []
        last_played_round = 1
        round_names = ["Huitièmes de finale", "Quarts de finale", "Demi-finales", "Match pour la 3ème place", "Finale"]
        
        for round_name in round_names:
            current_round = rounds.get(round_name=round_name)
            ordered_rounds.append(current_round)
            m_set = all_matches.filter(match_round__round_name=round_name)
            r_set = results.filter(match__match_round__round_name=round_name)
            # print(r_set)
            if len(r_set) > 0:
                last_played_round = current_round.id
            for m in m_set.all():
                # t1 = m.team1
                # t2 = m.team2
                r = None
                try:
                    r = results.get(match__match_round__round_name=round_name, match__match_id=m.id)
                    print("r", r, m)
                except Exception.DoesNotExist as e:
                    print("Exception m", e)
                    pass
                # print("Match in DB:", t1, "-", t2)
                if re.match("M[0-9]+", m.team1.name) != None:
                    m1_id = int(m.team1.name[1:])
                    m2_id = int(m.team2.name[1:])
                    try:
                        tmp_m1 = results.get(match__match_id=m1_id)
                        tmp_m2 = results.get(match__match_id=m2_id)
                        t1, _ = self.getMatchWinnerAndLoser(tmp_m1, matches)
                        t2, _ = self.getMatchWinnerAndLoser(tmp_m2, matches)
                        print("M ", m, "t1", t1, "-", "t2", t2, ":", tmp_m1, tmp_m2)
                        # print(matches)
                        # print(t1, "-", t2)                    
                    except Exception as e:
                        print(e, m)
                        pass
                elif re.match("L[0-9]+", m.team1.name) != None:
                    m1_id = int(m.team1.name[1:])
                    m2_id = int(m.team2.name[1:])
                    try:
                        tmp_m1 = results.get(match__match_id=m1_id)
                        # print(tmp_m1, tmp_m1.match.id)
                        tmp_m2 = results.get(match__match_id=m2_id)
                        _, t1 = self.getMatchWinnerAndLoser(tmp_m1, matches)
                        _, t2 = self.getMatchWinnerAndLoser(tmp_m2, matches)
                        # printLt1, "-", t2)
                        print("L ", t1, "-", t2, ":", r)
                    except Exception as e:
                        print(e, m)
                        pass
                elif re.match("[WR][A-H]", m.team1.name) != None:
                    t1, t2 = self.getGroupStageQualifiedTeams(m, rankings)
                    # print("WR", t1, "-", t2, ":", r)
                    if  r == None:
                        r = result_func(match = m,
                                        score_team1 = None,
                                        score_team2 = None,
                                        extratime = None,
                                        score_extratime_team1 = None,
                                        score_extratime_team2 = None,
                                        vainqueur = None
                                        )
                    r.match.team1 = t1
                    r.match.team2 = t2
                if t1 != -1:
                    m.team1 = t1
                    m.team2 = t2
                    if r == None:
                        r = result_func(match = m,
                                   score_team1 = None,
                                   score_team2 = None,
                                   extratime = None,
                                   score_extratime_team1 = None,
                                   score_extratime_team2 = None,
                                   vainqueur = None
                                )
                    else:
                        r.match.team1 = t1
                        r.match.team2 = t2
                    # print("Match after function", r.match)
                    matches.append(r)
        return matches, ordered_rounds, last_played_round
