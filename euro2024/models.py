from django.db import models
from datetime import datetime

class Winner(models.IntegerChoices):
        Team1 = 1
        Team2 = 2
        Draw = 3

# Create your models here.
# class User(models.Model):
#     name = models.CharField(max_length=20, null=False)
#     password = models.CharField(max_length=200)
#     def __str__(self):
#         return self.name

class Round(models.Model):
    round_name = models.CharField(max_length=200, null=False)
    extra_time_possible = models.BooleanField()
    def __str__(self):
        return str(self.id) + "||" + self.round_name

class Team(models.Model):
    name = models.CharField(max_length=50, null=True)
    iso2 = models.CharField(max_length=6, null=True)
    def __str__(self):
        return self.name

class Group(models.Model):
    name = models.CharField(max_length=1, null=True)
    team1 = models.ForeignKey(Team, null=False, related_name='team1', on_delete=models.CASCADE)
    team2 = models.ForeignKey(Team, null=False, related_name='team2', on_delete=models.CASCADE)
    team3 = models.ForeignKey(Team, null=False, related_name='team3', on_delete=models.CASCADE)
    team4 = models.ForeignKey(Team, null=False, related_name='team4', on_delete=models.CASCADE)
    def __str__(self):
        return self.name

class Match(models.Model):
    match_id = models.IntegerField(null=False)
    match_round = models.ForeignKey(Round, on_delete=models.CASCADE)
    team1 = models.ForeignKey(Team, null=False, related_name='home', on_delete=models.CASCADE)
    team2 = models.ForeignKey(Team, null=False, related_name='away', on_delete=models.CASCADE)
    match_date = models.DateTimeField('Date du match')
    def __str__(self):
        return str(self.match_id) + "||" + self.match_round.round_name + "||" + str(self.team1) + " - " + str(self.team2)

class Result(models.Model):
    match = models.ForeignKey(Match, on_delete=models.CASCADE, null=False)
    score_team1 = models.IntegerField()
    score_team2 = models.IntegerField()
    extratime = models.BooleanField()
    score_extratime_team1 = models.IntegerField(null=True)
    score_extratime_team2 = models.IntegerField(null=True)
    vainqueur = models.IntegerField(choices=Winner.choices, null=True)
    def __str__(self):
        return str(self.match.match_id) + "||" +  str(self.match.team1) + " - " + str(self.match.team2) + " || " + str(self.score_team1) +" - " + str(self.score_team2)

class Prono(models.Model):
    pronostiqueur = models.ForeignKey("auth.User", on_delete=models.CASCADE, null=False, related_name="euro2024Prono")
    prono_date = models.DateTimeField(default=datetime.now, blank=True)
    # result = models.ForeignKey(Result, on_delete=models.CASCADE)
    match = models.ForeignKey(Match, on_delete=models.CASCADE, null=False)
    score_team1 = models.IntegerField()
    score_team2 = models.IntegerField()
    extratime = models.BooleanField()
    score_extratime_team1 = models.IntegerField(null=True)
    score_extratime_team2 = models.IntegerField(null=True)
    vainqueur = models.IntegerField(choices=Winner.choices, null=True)
    def __str__(self):
        return str(self.pronostiqueur) + ": " + str(self.match.team1) + " - " + str(self.match.team2) + " || " + str(self.score_team1) +" - " + str(self.score_team2) + " @" +str(self.prono_date)
        # return str(self.pronostiqueur.name) + ": " + str(self.result)

