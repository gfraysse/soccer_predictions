from django.apps import AppConfig


class Euro2024Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'euro2024'
