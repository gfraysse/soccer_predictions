from django import template
from datetime import datetime, timedelta
from django.utils import timezone

register = template.Library()

@register.filter
def votetime_days(value):
    todays_date = timezone.now() + timedelta(minutes=60)
    then = value + timedelta(minutes = -135)
    if then <= todays_date:
        return 0
    delta = then - todays_date
    seconds = int(delta.total_seconds())
    nb_days = int(seconds / 60 / 60 / 24)
    result = ""
    if nb_days > 0:
        seconds -= nb_days * 60 * 60 * 24
        result = str(nb_days) + " days "
    hours = int(seconds / 60 / 60)
    if hours > 0:
        seconds -= hours * 60 * 60
    minutes = int(seconds / 60 )
    if minutes > 0:
        seconds -= minutes * 60

    result = result + "{:2}:{:2}:{:2}".format(hours, minutes, seconds)
    return result
