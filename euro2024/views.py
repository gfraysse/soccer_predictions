# -*- coding: utf-8 -*-
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.template import loader
from .models import Prono, Result, Team, Match, Round, Group, Winner
from django.contrib.auth.decorators import login_required
from django.db import models
from django.contrib.auth.models import User
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q


from datetime import datetime, timedelta
import functools
import re
from copy import deepcopy
########################################
@login_required
def teams(request):
    teams = Team.objects.order_by('name')
    t = []
    # Filter special teams for knockout stage
    for team in teams:
        if re.match("[LMT][0-9]*", team.name) == None and re.match("[WR][A-H]", team.name) == None:
            t.append(team)
    return render(request, request.resolver_match.app_name + '/teams.html', {
        'teams': t
    })

########################################
def __compare_rankings(r1, r2):
    # 1. Points obtained in all group matches;
    if r1.pts < r2.pts:
        return 1
    if r1.pts > r2.pts:
        return -1
    
    # From now on we assume both team have the same number of points
    assert(r1.pts == r2.pts)

    # 2. Goal difference in all group matches;
    if r1.gd < r2.gd:
        return 1
    if r1.gd > r2.gd:
        return -1
    
    # From now on we assume both team have the same goal difference
    assert(r1.gd == r2.gd)
    
    # 3. Number of goals scored in all group matches;
    if r1.gf < r2.gf:
        return 1
    if r1.gf > r2.gf:
        return -1

    #     Points obtained in the matches played between the teams in question;
    #     Goal difference in the matches played between the teams in question;
    #     Number of goals scored in the matches played between the teams in question;
    #     Fair play points in all group matches (only one deduction can be applied to a player in a single match):
    #         Yellow card: -1 point;
    #         Indirect red card (second yellow card): -3 points;
    #         Direct red card: -4 points;
    #         Yellow card and direct red card: -5 points;
    #     Drawing of lots.
    
    return 0
    
def __compute_group_rankings(results, matches, teams, groups):
    class Ranking:
        def __init__(self, team, group):
            self.team = team
            self.group = group
            self.gf = 0
            self.ga = 0
            self.gd = 0
            self.w = 0
            self.l = 0
            self.d = 0
            self.pld = 0
            self.pts = 0
            self.pos = 0
            
        def __str__(self):
            return self.team.name + ":" + str(self.pts)
    """
    class ThirdRanking:
        def __init__(self, team, group):
            self.team = team
            self.gf = 0
            self.ga = 0
            self.gd = 0
            self.w = 0
            self.l = 0
            self.d = 0
            self.pld = 0
            self.pts = 0
            self.pos = 0
            self.group = ""
        def __str__(self):
            return self.team.name + ":" + str(self.pts)
    """            

    r = []
    for group in groups.all():
        # print(group)
        ranking_group = {}
        for team in [group.team1, group.team2, group.team3, group.team4]:
            ranking_group[team.name] = Ranking(team, group)
        # print("ranking_group=", ranking_group)
        # for match in matches.all():
        for team in [group.team1, group.team2, group.team3, group.team4]:
            tmp = results.filter(match__team1_id=team.id)
            # print("  match=", match)
            #     print("    team=", team)
            for result in tmp:
                try:
                    # if match.team1.name != team.name:
                    #     continue
                    # result = results.get(match__team1_id=team.id)
                    # print("      result=", result)
                    ranking_group[result.match.team1.name].gf += result.score_extratime_team1
                    ranking_group[result.match.team1.name].ga += result.score_extratime_team2
                    ranking_group[result.match.team1.name].gd += result.score_extratime_team1 - result.score_extratime_team2

                    ranking_group[result.match.team2.name].gf += result.score_extratime_team2
                    ranking_group[result.match.team2.name].ga += result.score_extratime_team1
                    ranking_group[result.match.team2.name].gd += result.score_extratime_team2 - result.score_extratime_team1

                    ranking_group[result.match.team1.name].pld += 1
                    ranking_group[result.match.team2.name].pld += 1

                    if result.score_extratime_team1 > result.score_extratime_team2:
                        ranking_group[result.match.team1.name].w += 1
                        ranking_group[result.match.team1.name].pts += 3
                        ranking_group[result.match.team2.name].l += 1
                    elif result.score_extratime_team1 < result.score_extratime_team2:
                        ranking_group[result.match.team2.name].w += 1
                        ranking_group[result.match.team2.name].pts += 3
                        ranking_group[result.match.team1.name].l += 1
                    else: 
                        ranking_group[result.match.team2.name].d += 1
                        ranking_group[result.match.team2.name].pts += 1
                        ranking_group[result.match.team1.name].d += 1
                        ranking_group[result.match.team1.name].pts += 1
                           

                except Result.DoesNotExist as e:
                    continue
                
        for team in [group.team1, group.team2, group.team3, group.team4]:
            r.append(ranking_group[team.name])

    for group in groups.all():
        group_ranking = {}
        for team in [group.team1, group.team2, group.team3, group.team4]:
            for ranking in r:
                if ranking.team.name == team.name:
                    group_ranking[team.name] = ranking
        decorated = [(g.pts, i, g) for i, g in enumerate(group_ranking.values())]
        gr = sorted(group_ranking.values(), key=functools.cmp_to_key(__compare_rankings))
        # decorated.sort(reverse=True)
        # print(decorated)
        # gr = [gr for pts, i, gr in decorated]
        # print("gr=", gr)
        # for k, v in gr.items():
        for i in range(len(gr)):
            # print("group i=", v.team.name)
            team_name = gr[i].team.name
            # print("group i=", team_name)
            group_ranking[team_name].pos = i + 1

    rankings = deepcopy(r)
    # manage thirds teams to compute ranking of best thirds
    thirds_teams = []
    for group in groups.all():
        for _r in rankings:
            if _r.group.name == group.name and _r.pos == 3:
                thirds_teams.append(_r)
                

    decorated = [(g.pts, i, g) for i, g in enumerate(thirds_teams)]
    thirds_teams = sorted(thirds_teams, key=functools.cmp_to_key(__compare_rankings))
    # for i in range(len(gr)):
    #     team_name = gr[i].team.name
    #     thirds_teams[team_name].pos = i + 1
    # for k, v in thirds_teams.items():
    #     print(v.pos, k)
    i = 1
    for t in thirds_teams:
        t.pos = i
        i += 1
        # print(t.pos, t)

    return r, thirds_teams

########################################
@login_required
def resultats(request):
    results = Result.objects
    matches = Match.objects
    teams = Team.objects
    groups = Group.objects

    rankings, thirds_teams = __compute_group_rankings(results, matches, teams, groups)
    # print("rankings", rankings)
    # print("thirds_teams", thirds_teams)
    return render(request, request.resolver_match.app_name + '/resultats.html', {
        'teams': teams.order_by('name'),
        'groups': groups.order_by('name'),
        'results': results,
        'groups': Group.objects,
        'rankings': rankings,
        'thirds_teams': thirds_teams,
    })

########################################
@login_required
def groups(request):
    return render(request, request.resolver_match.app_name + '/groups.html', {
        'teams': Team.objects.order_by('name'),
        'groups': Group.objects.order_by('name')
    })

def __compute_points_for_user_on_result(prono, result):
    points = 0
    # print("match", prono.match, ", prono=", prono, ", result ", result)
    if result.score_team1 > result.score_team2 or (result.extratime == True and result.score_extratime_team1 > result.score_extratime_team2):
        match_winner = 1
    elif result.score_team1 < result.score_team2 or (result.extratime == True and result.score_extratime_team1 < result.score_extratime_team2):
        match_winner = 2
    else:
        match_winner = 3

    # print(match_winner, prono.vainqueur)
    if match_winner == prono.vainqueur:
        # Has identifier the right winner and the right score: 15 points
        if (result.score_team1 == prono.score_team1 and result.score_team2 == prono.score_team2) or (result.extratime == prono.extratime and (result.score_extratime_team1 == prono.score_extratime_team1 and result.score_extratime_team2 == prono.score_extratime_team2)):
            # print("15 points for right score for match", prono.match)
            return 15
        # right winner : 5 points
        # print("5 points for winner for match", prono.match)
        points = 5
    else:
        points = 0
        return points
    
    goal_diff_real = abs(result.score_team1 - result.score_team2)
    if result.extratime == True:
        goal_diff_real = abs(result.score_extratime_team1 - result.score_extratime_team2)
    goal_diff_prono = abs(prono.score_team1 - prono.score_team2)
    if prono.extratime == True:
        goal_diff_prono = abs(prono.score_extratime_team1 - prono.score_extratime_team2)

    if goal_diff_prono == goal_diff_real:
        points_for_goal_diff = 4 - abs(result.score_team1 - prono.score_team1)
        if points_for_goal_diff > 0:
            points += points_for_goal_diff
            # print(points_for_goal_diff, "points for goal diff for ", prono.match)

    points_for_goal_team1 = 2 - abs(result.score_team1 - prono.score_team1)
    if points_for_goal_team1 > 0:
        points += points_for_goal_team1
        # print(points_for_goal_team1, "points for goal team1 for ", prono.match)
        
    points_for_goal_team2 = 2 - abs(result.score_team2 - prono.score_team2)
    if points_for_goal_team2 > 0:
        points += points_for_goal_team2
        # print(points_for_goal_team2, "points for goal team2 for ", prono.match)
    
    return points

########################################
@login_required
def classement(request):
    results = Result.objects
    pronos = Prono.objects
    
    # only display user with at least one pronostic in DB
    _active_users = Prono.objects.values('pronostiqueur__username').distinct()
    # print("users", _active_users)
    users = User.objects.filter(username__in = _active_users)
    
    result_pronos = []
    user_points = []
    points_dict = {}
    class UserTotalPoints:
        def __init__(self, user):
            self.user = user
            self.points = {}
            self.total_points = 0
        def __str__(self):
            return self.user + " " + str(self.total_points)
        
    for u in users.all():
        tmp = UserTotalPoints(u.get_username())
        user_points.append(tmp)
        points_dict[u.get_username()] = tmp
    class ResultatProno:
        def __init__(self, prono):
            self.user = prono.pronostiqueur.get_username()
            self.match_id = prono.match.match_id
            self.points = 0
            self.prono = prono
        def __str__(self):
            return self.user + " " + str(self.match_id) + " " + str(self.points)
            
    for prono in pronos.all():
            r = ResultatProno(prono)
            try:            
                resultat = results.get(match__match_id=prono.match.match_id)
                points = __compute_points_for_user_on_result(prono, resultat)
                r.points += points
                # print("User", prono.pronostiqueur, "got ", points, "points for match", prono.match)
                # print("prono was", prono)
                points_dict[prono.pronostiqueur.get_username()].total_points += points
                if resultat.match.match_round.id in points_dict[prono.pronostiqueur.get_username()].points.keys():
                    points_dict[prono.pronostiqueur.get_username()].points[resultat.match.match_round.id] += points
                else:
                    points_dict[prono.pronostiqueur.get_username()].points[resultat.match.match_round.id] = points
            except Result.DoesNotExist as e:
                # print(e)
                pass
            result_pronos.append(r)

    rankings, thirds_teams = __compute_group_rankings(Result.objects, Match.objects, Team.objects, Group.objects)
    matches, ordered_rounds, last_played_round = __compute_knockout_stages(Round.objects, Result.objects, rankings, thirds_teams)
    
    # fill the array to have an entry per user and per match that has been played, so that the page can be displayed easily
    for _user in users.all():
        # print("checking user", _user)
        for _result in results.all():
            found = False
            # print("checking if all pronos found for result", _result, "id: ", _result.match_id)
            for _result_prono in result_pronos:
                # print("  checking prono", _result_prono)
                # print("  checking prono users", _result_prono.user, _user)
                if _result_prono.match_id == _result.match_id and _result_prono.user == _user.username:
                    # print("  FOUND prono for match", _result, "by user", _user)
                    found = True
                    break

            if not found:
                print("no prono for match", _result, "by user", _user, ", creating mock empty prono for display")
                _prono = Prono(pronostiqueur = _user,
                               match = _result.match)
                r = ResultatProno(_prono)
                result_pronos.append(r)
    
    return render(request, request.resolver_match.app_name + '/classement.html', {
        'result_pronos': result_pronos,
        # 'rounds': Round.objects.filter(Q(round_name="Phase de poule") | Q(round_name="Huitièmes de finale")),
        'rounds': Round.objects,
        'current_prediction_round': last_played_round,
        'results': results.order_by('match_id'),
        'matches': matches,#.order_by('match_id'),
        'users': users,
        'users_totalpoints': user_points
    })

########################################
@login_required
@csrf_exempt
def pronostics(request):
    results = Result.objects
    teams = Team.objects
    groups = Group.objects
    rounds = Round.objects

    m = Match.objects
    # First : compute standings of group stage to define the knockout rounds matches
    rankings, thirds_teams = __compute_group_rankings(results, m, teams, groups)    
    # print("pronostics rankings", rankings)
    _matches, ordered_rounds, last_played_round = __compute_knockout_stages(rounds, results, rankings, thirds_teams)
    # print("matches", _matches)
    matches = []
    for _m in _matches:
        r = Result()
        r.match = _m.match
        matches.append(r)

    # last_played_round += 1
    
    # Then: add first round matches
    ordered_rounds.insert(0, rounds.get(round_name="Phase de poule"))
    matches_group_stage = Match.objects.filter(match_round__round_name="Phase de poule")
    for _m in matches_group_stage:
        r = Result()
        r.match = _m
        matches.append(r)

    user = request.user.get_username()
    pronos = Prono.objects.filter(pronostiqueur__username=user)
    user_pronos = []
    # print("pronostics=", pronos)
    for m in Match.objects.all():
        try:            
            p = pronos.get(match__id=m.id)
            # print("p=", p)
            user_pronos.append(p)
        except Prono.MultipleObjectsReturned as e:
            print(m, e)
        except Prono.DoesNotExist as e:
            # print(m)
            newp = Prono(
                pronostiqueur=request.user,
                match=m,
            )
            # print("newp=", newp)
            user_pronos.append(newp)
    return render(request, request.resolver_match.app_name + '/pronostics.html', {
        'user': user,
        'rounds': Round.objects,
        'matches': matches,
        'pronostics': user_pronos,
        'current_prediction_round': last_played_round
    })

########################################
@login_required
@ensure_csrf_cookie
def pronostiquer(request):
    results = Result.objects
    teams = Team.objects
    groups = Group.objects
    rounds = Round.objects

    m = Match.objects
    # First : compute standings of group stage to define the knockout rounds matches
    rankings, thirds_teams = __compute_group_rankings(results, m, teams, groups)    
    _matches, ordered_rounds, last_played_round = __compute_knockout_stages(rounds, results, rankings, thirds_teams)
    
    matches = []
    for _m in _matches:
        r = Result()
        r.match = _m.match
        matches.append(r)
        
    # Then: add group stage matches to list
    ordered_rounds.insert(0, rounds.get(round_name="Phase de poule"))
    matches_group_stage = Match.objects.filter(match_round__round_name="Phase de poule")
    for match in matches_group_stage:
        r = Result()
        r.match = match
        matches.append(r)
        
    user = request.user.get_username()
    pronos = Prono.objects.filter(pronostiqueur__username=user)
    user_pronos = []
    # print("pronostics=", pronos)
    for m in Match.objects.all():
        try:            
            p = pronos.get(match__match_id=m.match_id)
            # print("p=", p)
            user_pronos.append(p)
        except Prono.DoesNotExist as e:
            # print(m)
            newp = Prono(
                pronostiqueur=request.user,
                match=m,
            )
            # print("newp=", newp)
            user_pronos.append(newp)
    # print("user_pronos", user_pronos)
    todays_date = datetime.now() + timedelta(minutes=135)
    # print(todays_date)
    # print("pronostiquer matches", matches)
    return render(request, request.resolver_match.app_name + '/pronostiquer.html', {
        'user': user,
        'rounds': Round.objects, #.filter(round_name="Phase de poule"),
        # 'matches': Match.objects.order_by('match_id'),
        'pronostics': user_pronos,
        'todays_date': todays_date,        
        'matches': matches,
        'ordered_rounds': ordered_rounds,
        'current_prediction_round': last_played_round + 1
    })

def record(request):
    # match = get_object_or_404(Match, pk=match_id)
    # try:
    # match_choice = match.choice_set.get(pk=request.POST['score1'])
    # print(request.POST)
    match_score1 = request.POST.getlist('score1')
    match_score2 = request.POST.getlist('score2')
    match_ids = request.POST.getlist('matchid')
    # print("match_score1 =", match_score1)
    # print("match_score2 =", match_score2)
    # print("match_ids =", match_ids)
    for i in range(len(match_ids)):
        idx = int(match_ids[i])
        # print("idx=", idx)
        if match_score1[i] != '' and match_score2[i] != '':
            m = Match.objects.get(id=idx) # not match_id
            if match_score1[i] > match_score2[i]:
                vainqueur = 1
            elif match_score1[i] < match_score2[i]:
                vainqueur = 2                    
            else:
                vainqueur = 3
            # print(i, idx, m)
            try:
                user = request.user.get_username()           
                p = Prono.objects.get(pronostiqueur__username=user, match__id=idx)
                p.match = m
                p.score_team1 = match_score1[i]
                p.score_team2 = match_score2[i]
                p.score_extratime_team1 = match_score1[i]
                p.score_extratime_team2 = match_score2[i]
                p.extratime = False
                p.vainqueur = vainqueur
                p.save()
                # print("1 p=", p)
            except Exception as e:
                print("Exception while creating prono", e)
                p = Prono(
                    pronostiqueur=request.user,
                    # prono_date=models.DateTimeField(default=datetime.now, blank=True),
                    match=m,
                    score_team1=match_score1[i],
                    score_team2=match_score2[i],
                    extratime=False,                
                )
                # print("p=", p)
                p.score_extratime_team1 = match_score1[i]
                p.score_extratime_team2 = match_score2[i]
                p.extratime = False
                p.vainqueur = vainqueur
                p.save()
    return pronostics(request) #render(request, request.resolver_match.app_name + '/pronostics.html', {
    #})

def __getMatchWinnerAndLoser(result, matches):
    # Team name is of format Mxy whre xy is the match number
    match_id = result.match.match_id
    try:
        for m in matches:
            # print("match_id=", m.match.match_id, match_id)
            if m.match.match_id == match_id:
                # print(m)
                if result.vainqueur == Winner.Team1:
                    return m.match.team1, m.match.team2
    
                return m.match.team2, m.match.team1
    except Result.DoesNotExist as e:
        return -1, -1

def __getGroupStageQualifiedTeams(match, rankings, thirds_teams):
    # Team name is of format [WR]G where:
    # - W stand for Winner of the group
    # - R stand for Runner-up of the group
    # - Tx means the xth best third teams
    # - G is the Group name
    round_of_16_dict = {
        "ABCD": "ADBC",
        "ABCE": "AEBC",
        "ABCF": "AFBC",
        "ABDE": "DEAB",
        "ABDF": "DFAB",
        "ABEF": "EFBA",
        "ACDE": "EDCA",
        "ACDF": "FDCA",
        "ACEF": "EFCA",
        "ADEF": "EFDA",
        "BCDE": "EDBC",
        "BCDF": "FDCB",
        "BCEF": "FECB",
        "BDEF": "FEDB",
        "CDEF": "FEDC"
        }
    g1 = match.team1.name[1]
    g2 = match.team2.name[1]
    t1 = -1
    t2 = -1
    group_thirds = ["0" for _ in range(4)]
    for t in thirds_teams:
        if t.pos < 5:
            group_thirds[t.pos - 1] = t.group.name
    group_thirds_str = "".join(sorted(group_thirds))
    # print("group_thirds", group_thirds_str)

    # all matches involiving third teams are in the Wx-Ty format never Ty-Wx
    if match.team2.name[0] == "T":
        if g2 >= "0" and g2 <= "9":
            if g1 == "B":
                g2 = round_of_16_dict[group_thirds_str][0]
            elif g1 == "C":
                g2 = round_of_16_dict[group_thirds_str][1]
            elif g1 == "E":
                g2 = round_of_16_dict[group_thirds_str][2]
            elif g1 == "F":
                g2 = round_of_16_dict[group_thirds_str][3]
        else:
            print("error", match.team2)
        
    
    for r in rankings:
        if r.group.name == g1:
            if (match.team1.name[0] == "W" and r.pos == 1) or (match.team1.name[0] == "R" and r.pos == 2):
                t1 = r.team
                # print(match, "t1=", t1)
        elif r.group.name == g2:
            if (match.team2.name[0] == "W" and r.pos == 1) or (match.team2.name[0] == "R" and r.pos == 2):
                t2 = r.team
                # print(match, "t2=", t2)
            elif match.team2.name[0] == "T" and r.pos == 3:
                t2 = r.team
                # print(match, "T t2=", t2)
                
        if t1 != -1 and t2 != -1:
            break

    assert t1 != t2
    assert t1 != -1
    assert t2 != -1
    return t1, t2

def __compute_knockout_stages(rounds, results, rankings, thirds_teams):
    matches = []
    ordered_rounds = []
    last_played_round = 1
    round_names = ["Huitièmes de finale", "Quarts de finale", "Demi-finales", "Finale"]
    for _round_name in round_names:
        current_round = rounds.get(round_name=_round_name)
        ordered_rounds.append(current_round)
        r_set = list(results.filter(match__match_round__round_name=_round_name))
        if len(r_set) > 0:
            last_played_round = current_round.id

        # Adding unplayed match to current round
        _match_set = Match.objects.filter(match_round__round_name=_round_name)

        for _m in _match_set:
            found = False
            for _r in r_set:
                if _m.match_id == _r.match.match_id:
                    found = True
                    break
            if found == False:
                # print("adding", _m)
                r = Result()
                r.match = _m
                r_set.append(r)
        
        for m in r_set:
            t1 = m.match.team1
            t2 = m.match.team2
            r = None
            try:
                r = results.get(match__match_round__round_name=_round_name, match__match_id=m.match.id)
            except Result.DoesNotExist as e:
                pass
            # print(t1, "-", t2)
            if re.match("M[0-9]+", m.match.team1.name) != None:
                m1_id = int(m.match.team1.name[1:])
                m2_id = int(m.match.team2.name[1:])
                # print(m, m1_id, m2_id)
                try:
                    tmp_m1 = Result.objects.get(match__match_id=m1_id)
                    # print(tmp_m1, tmp_m1.match.match_id)
                    tmp_m2 = Result.objects.get(match__match_id=m2_id)
                    # print(tmp_m2, tmp_m2.match.match_id)
                    t1, _ = __getMatchWinnerAndLoser(tmp_m1, matches)
                    t2, _ = __getMatchWinnerAndLoser(tmp_m2, matches)
                    # print(t1, "-", t2)
                except Result.DoesNotExist as e:
                    pass
            elif re.match("L[0-9]+", m.match.team1.name) != None:
                m1_id = int(m.match.team1.name[1:])
                m2_id = int(m.match.team2.name[1:])
                try:
                    tmp_m1 = Result.objects.get(match__match_id=m1_id)
                    # print(tmp_m1, tmp_m1.match.match_id)
                    tmp_m2 = Result.objects.get(match__match_id=m2_id)
                    _, t1 = __getMatchWinnerAndLoser(tmp_m1, matches)
                    _, t2 = __getMatchWinnerAndLoser(tmp_m2, matches)
                    # print(t1, "-", t2)
                except Result.DoesNotExist as e:
                    pass
            elif re.match("L[0-9]+", m.match.team1.name) != None:
                m1_id = int(m.match.team1.name[1:])
                m2_id = int(m.match.team2.name[1:])
                try:
                    tmp_m1 = Result.objects.get(match__match_id=m1_id)
                    # print(tmp_m1, tmp_m1.match.match_id)
                    tmp_m2 = Result.objects.get(match__match_id=m2_id)
                    _, t1 = __getMatchWinnerAndLoser(tmp_m1, matches)
                    _, t2 = __getMatchWinnerAndLoser(tmp_m2, matches)
                    # print(t1, "-", t2)
                except Result.DoesNotExist as e:
                    pass
            elif re.match("[WR][A-H]", m.match.team1.name) != None or re.match("T[0-9]+", m.match.team1.name) != None:
                t1, t2 = __getGroupStageQualifiedTeams(m.match, rankings, thirds_teams)
            if t1 != -1:
                m.match.team1 = t1
                m.match.team2 = t2
                matches.append(m)
    return matches, ordered_rounds, last_played_round

@login_required
def knockout(request):
    results = Result.objects
    teams = Team.objects
    groups = Group.objects
    rounds = Round.objects

    m = Match.objects
    rankings, thirds_teams = __compute_group_rankings(results, m, teams, groups)
    matches, ordered_rounds, last_played_round = __compute_knockout_stages(rounds, results, rankings, thirds_teams)
    # print("matches", matches)
    # print("ordered_rounds", ordered_rounds)
    # print("last_played_round", last_played_round)
    return render(request, request.resolver_match.app_name + '/knockout.html', {
        # 'teams': teams.order_by('name'),
        'results': results,
        'matches': matches,
        'rounds': rounds,
        'ordered_rounds': ordered_rounds,
        'current_prediction_round': last_played_round
    })


# UPDATE `sqlite_sequence` SET `seq` = (SELECT MAX(`id`) FROM "pronostics_prono") WHERE `name`="pronostics_prono";
# sqlite> select * from pronostics_prono;
# https://docs.djangoproject.com/en/4.1/ref/templates/builtins/#date
# https://flagpedia.net/download/api
