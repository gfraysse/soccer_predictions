# -*- coding: utf-8 -*-
from django.test import TestCase
from django.urls import reverse

from euro2024.models import Winner, Group, Team, Round, Match, Result, Prono 
from . import views
from .views import __compute_group_rankings as compute_group_rankings
from .views import  __compute_knockout_stages as compute_knockout_stages

# python ./manage.py test euro2024.tests.Euro2024TestCase
class Euro2024TestCase(TestCase):
    def setUp(self):
        Team.objects.create(name="France", iso2="fr")
        Team.objects.create(name="Spain", iso2="es")

    def match_creation(self):
        """ Creation of teams and match is OK"""
        team1 = Team.objects.get(name="France")
        team2 = Team.objects.get(name="Spain")
        r = Round.objects.create(round_name = "Phase de poule", extra_time_possible = False)
        m = Match.objects.create(match_id=1, match_round = r, team1 = team1, team2 = team2, match_date="2012-01-01 18:00")
        result = Result.objects.create(match = m, score_team1 = 0, score_team2 = 1, extratime = False, score_extratime_team1 = 0, score_extratime_team2 = 1, vainqueur = Winner.Team2)
        self.assertEqual(result.vainqueur, Winner.Team2)

    def test_pronostics_view(self):
        # w = self.create_whatever()
        url = reverse("euro2024:pronostics")
        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 302)
        # self.assertIn("Euro2024", resp.content)
        
    def test_knockout_stage(self):
        results = Result.objects
        teams = Team.objects
        groups = Group.objects
        rounds = Round.objects
        print("rounds", rounds.all())
        print("results", results.all())
        # rankings, thirds_teams = compute_group_rankings(results, Match.objects, teams, groups)
        # matches, ordered_rounds, last_played_round = compute_knockout_stages(rounds, results, rankings, thirds_teams)
        # print(matches)
